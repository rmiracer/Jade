#include "headers.h"

/**
*  Open output files
@param[in] fp1  File pointer (will be assigned to "fourier_space.dat")
@param[in] fp2  File pointer (will be assigned to "real_space.dat")
@param[in] fp3  File pointer (will be assigned to "tracer.dat")
@param[in] fp4  File pointer (will be assigned to "poincare_points.dat")
@param[in] fp5  File pointer (will be assigned to "vorticity.dat")
@return 1 if files sucessfully open; 0 otherwise
*/
int open_output_files(FILE **fp1, FILE **fp2, FILE **fp3, FILE **fp4, FILE **fp5, int my_pid)
{
  if(my_pid == 0)
  {
    if(!(*fp1 = fopen("fourier_space.dat", "w")))
    {
      fprintf(stderr, "Error opening output file fourier_space.dat\n");
      return 0;
    }
    if(!(*fp2 = fopen("real_space.dat", "w")))
    {
      fprintf(stderr, "Error opening output file vector_real_space.dat\n");
      return 0;
    }
    if(!(*fp3 = fopen("tracer.dat", "w")))
    {
      fprintf(stderr, "Error opening output file tracer.dat\n");
      return 0;
    }
    if(!(*fp4 = fopen("poincare_points.dat", "w")))
    {
      fprintf(stderr, "Error opening output file poincare_points.dat\n");
      return 0;
    }
    if(!(*fp5 = fopen("vorticity.dat", "w")))
    {
      fprintf(stderr, "Error opening output file vorticity.dat\n");
      return 0;
    }
  }
  return 1;
}

#ifdef WITH_TRACERS
/**
 Initialize the initial positions of tracers
@param[out] x0  Initial X position of tracers
@param[out] y0  Initial Y position of tracers
*/
void initialize_tracer_positions(double x0[NUM_TRACERS], double y0[NUM_TRACERS])
{
  int i, j;

  for(i = 0; i < M*M; i++)
  {
    for(j = 0; j < N*N; j++)
    {
      x0[i*N*N + j] = ((double)i)/M;
      y0[i*N*N + j] = ((double)j)/N;
    }
  }
}
#endif //WITH_TRACERS

/**
   Initialization of the unit 'polarization' vectors e_k
   (following Lee (1987))
@param[out] e  Matrix of size MAX_KX*MAX_KY*2
*/
void initialize_e_vectors(double e[MAX_KX][MAX_KY][2])
{
  int i, j, k;
  int k1, k2;

  double norm;

  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      for(k = 0; k < 2; k++)
      {
//        e[i][j][k] = -100.;
        e[i][j][k] = 0.;
      }
    }
  }

  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      // k vector
      k1 = i;
      k2 = j - MAX_KY/2;


      if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
      {
        // Orthogonality
        e[i][j][0] = k2;
        e[i][j][1] = -k1;

        // Unitary
        norm = sqrt(k1*k1 + k2*k2);

        e[i][j][0] = e[i][j][0]/norm;
        e[i][j][1] = e[i][j][1]/norm;

      }
    }
  }
}

/**
   Set the value of the forcing term
@param[out] f  Complex matrix MAX_KX*MAX_KY*2 representing the external forcing on each Fourier mode
@return 1 in all cases
*/
int initialize_control_parameter(double f[MAX_KX][MAX_KY][2])
{
  int i, j, l, k1, k2;
  double fk;

  fk = FORCING_AMPLITUDE;

  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      // Real and imaginary parts loop
      // Forcing used in Feudel & Seehafer CSF 1995
      for(l = 0; l < 2; l++)
      {
        k1 = i;
        k2 = j - MAX_KY/2;

        if(k1 == 4 && k2 == 1)
        {
          f[i][j][l] = fk;
        }
        else
        {
          f[i][j][l] = 0.;
        }
      }
/*
        // Initial condition by Feudel & Seehafer, PRE 1998
        k1 = i;
        k2 = j - MAX_KY/2;
        if(k1 == 1)
        {
          if(k2 == -2)
          {
            f[i][j][0] = 0.1*1.16702;
            f[i][j][1] = 0.1*-9.038995;
          }
          else if(k2 == 2)
          {
            f[i][j][0] = 0.1*8.676419;
            f[i][j][1] = 0.1*1.44072;
          }
        }
*/
    }
  }

  return 1;
}

#if 0
// Testing
void initialize_velocity_field(double vx_real_space[M][N], double vy_real_space[M][N])
{
  int i, j;
  int k1, k2;

  double ax, bx, ay, by;
  double Dx, Dy;

  // Boundaries
  ax = 0.;
  bx = 2.*M_PI;
  ay = 0.;
  by = 2.*M_PI;

  Dx = (bx - ax)/(double)M;
  Dy = (by - ay)/(double)N;

  k1 = 2.;
  k2 = 1.;

  for(i = 0; i < M; i++)
  {
    for(j = 0; j < N; j++)
    {
      vx_real_space[i][j] = 0.1*sin(k1*(i+1)*Dx)*cos(k2*(j+1)*Dx);
      vy_real_space[i][j] = -0.1*cos(k1*(i+1)*Dx)*sin(k2*(j+1)*Dx);
    }
  }

}
#endif

/**
  Shift a complex matrix for FFTW.
@param[in] m1  Matrix to be shifted
@param[out] m2  Shifted matrix
*/
void shift_matrix(double m1[M][N/2+1][2], double m2[M][N/2+1][2])
{
  int i, j;

  for(i = 0; i < M; i++)
  {
    for(j = 0; j < N/2+1; j++)
    {
      m2[(i + (int)(M/2))%M][j][0] = m1[i][j][0];
      m2[(i + (int)(M/2))%M][j][1] = m1[i][j][1];
    }
  }
}

#if 0
// Testing
void set_initial_conditions(double v[MAX_KX][MAX_KY][2], double e[MAX_KX][MAX_KY][2])
{
  int i, j;
  double vx[M][N], vy[M][N];
  double vx_fourier_space[M][N/2+1][2], vy_fourier_space[M][N/2+1][2];
  double matrix1_temp[M][N/2+1][2], matrix2_temp[M][N/2+1][2];

#ifdef SAVE_REAL_SPACE
  fftw_plan p1, p2;

  p1 = fftw_plan_dft_r2c_2d(M, N, &vx[0][0], &matrix1_temp[0][0], FFTW_ESTIMATE);
  p2 = fftw_plan_dft_r2c_2d(M, N, &vy[0][0], &matrix2_temp[0][0], FFTW_ESTIMATE);

  initialize_velocity_field(vx, vy);

  for(i = 0; i < M; i++)
  for(j = 0; j < N; j++)
  {
    matrix1_temp[i][j][0] = 0.;
    matrix1_temp[i][j][1] = 0.;
  }

  fftw_execute(p1);
  fftw_execute(p2);

  shift_matrix(matrix1_temp, vx_fourier_space);
  shift_matrix(matrix2_temp, vy_fourier_space);

  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      v[i][j][0] = vx_fourier_space[j][i][0]*e[i][j][0] + vy_fourier_space[j][i][0]*e[i][j][1];
      v[i][j][1] = vx_fourier_space[j][i][1]*e[i][j][0] + vy_fourier_space[j][i][1]*e[i][j][1];
    }
  }

#endif //SAVE_REAL_SPACE
}
#endif

/**
   Set the values of the initial conditions from an ASCII file
@param[out] v  Matrix of size MAX_KX*MAX_KY*2 representing the real and imaginary parts of the complex coefficients in Fourier space.
@return 1 in all cases (should depend on whether the file was correctly opened or not).
*/
int read_initial_conditions(double v[MAX_KX][MAX_KY][2])
{
  int i, j, l, k1, k2;
  double buffer;
  FILE *fp;

  fp = fopen("initial_conditions.dat", "r");

  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      for(l = 0; l < 2; l++)
      {
        k1 = i;
        k2 = j - MAX_KY/2;

        if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
        {
	  fscanf(fp, "%lf", &v[i][j][l]);
        }
        else
        {
//            v[i][j][l] = -100.;
            v[i][j][l] = 0.;
        }
      }
    }
  }

  fclose(fp);

  return 1;
}

/**
   Set the values of the initial conditions from a binary file
@param[out] tr  Pointer to double representing the time of initial condition.
@param[out] v  Matrix of size MAX_KX*MAX_KY*2 representing the real and imaginary parts of the complex coefficients in Fourier space.
@return 1 in all cases (should depend on whether the file was correctly opened or not).
*/
double load(double *tr, double matrix[MAX_KX][MAX_KY][2])
{
  int i, j, index;
  double t;
  FILE *fp;

  fp = fopen("new_initcond.dat", "r");

  t = 0.;
//  while(!feof(fp))
//  while(t < 253600)
//  for(index = 0; index < NUM_POINTS_ATTRACTOR; index++)
  {
    fread(&t, sizeof(double), 1, fp);
    fread((void *)(matrix), sizeof(double), MAX_KX*MAX_KY*2, fp);

  }

  printf("Using point at t = %lf\n", t);
//    printf("matrix[4][1] = %12.11lf\n", matrix[8][2+MAX_KY/2][0]);
  fclose(fp);

//  printf("t = %lf\n", t);
//  return t;
    *tr = t;
}
