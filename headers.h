/**
  Constants for JADE
 */

#ifndef HEADERS_H
#define HEADERS_H

#include <stdio.h>
#include <math.h>

#include <fftw3.h>

#include <mpi.h>

#include "config.h"

// Dimensions of the Z^2 Subspace
// See Fig. 1 from Lee (1995)
#define MAX_KX 9
#define MAX_KY 17
#define TRUNCATION 8

// Dimension of the (whole) Z^2 space
#define M 512
#define N (M)

// Dimensions of an auxiliary matrix
// needed to perform the inverse FT
#define MAX_KX_INTERP (M/2 + 1)
#define MAX_KY_INTERP (M)

#define MAX_TIME 20000.
#define TRANSIENT_TIME 10000.

#define STEP 1.e-2
#define TRANSIENTS_STEP 1.e-1

// Reynolds number
#define Re 51.6

// Amplitude of forcing mode (currently set to (kx, ky) = (4, 1))
#define FORCING_AMPLITUDE 0.13666

#define SAVE_FOURIER_SPACE

// Define this if you want the solution in real space.
//#define SAVE_REAL_SPACE

// Use FFTW 3
// SAVE_REAL_SPACE must be defined too
#define WITH_FFTW

// Use our own, home-brewed, super-slow FT code
// SAVE_REAL_SPACE must be defined too
//#define WITH_DFT

// Define this if you want to set a Poincare hypersurface of section
//#define CALCULATE_POINCARE_POINTS

//#define NUM_TRACERS M*M*N*N
//#define NUM_TRACERS 1

#endif // HEADERS_H
