#include "headers.h"

/**
   Definition of the subspace as described by Feudel & Seehafer (1995)
   K^2 := {k e Z^2 : k1 > 0} U {k e Z^2 : k1 = 0 & k2 > 0}
@param[in] k1 X component of wavevector (aka kx)
@param[in] k1 Y component of wavevector (aka ky)
@return 1 if wavevector is in subspace, 0 otherwise
*/
int is_in_subspace(int k1, int k2)
{
  if((k1 > 0) || (k1 == 0 && k2 > 0))
    return 1;
  return 0;
}

/**
   Definition of the truncation in the K^2 space by Feudel & Seehafer (1995)
   The K^2 subspace is segmented into successive rings defined by
   n^2 - n < k^2 <= n^2 + n
@param[in] k1 X component of wavevector (aka kx)
@param[in] k1 Y component of wavevector (aka ky)
@return 1 if wavevector is within truncation, 0 otherwise
*/
int is_in_truncation(int k1, int k2)
{
  if(k1*k1 + k2*k2 <= TRUNCATION*TRUNCATION + TRUNCATION)
    return 1;
  return 0;
}

/**
   Bracket function as defined by Feudel & Seehafer (1995)
   q1 & q2 are pointers because we need to overwrite its values
@param[out] q1 X component of output wavevector (aka kx)
@param[out] q2 Y component of output wavevector (aka ky)
@param[in] k1 X component of input wavevector (aka kx)
@param[in] k2 Y component of input wavevector (aka ky)
*/
void bracket(int *q1, int *q2, int k1, int  k2)
{

  if(is_in_subspace(k1, k2))
  {
    *q1 = k1;
    *q2 = k2;
  }
  else
  {
    *q1 = -k1;
    *q2 = -k2;
  }
}

/**
   sgn function as defined by Feudel & Seehafer (1995)
@param[in] k1 X component of input wavevector (aka kx)
@param[in] k2 Y component of input wavevector (aka ky)
@return Value of sgn function.
*/
double sgn(k1, k2)
{
  int k1_temp, k2_temp;
  double sign;
  int module;

  bracket(&k1_temp, &k2_temp, k1, k2);

  module = k1*k1 + k2*k2;

  sign = (k1*k1_temp + k2*k2_temp)/module;

  return sign;
}

/**
   The infamous derivs routine.
   It evaluates the equations (16) & (17)
   from Feudel & Seehafer (1995) in three parts:
   First, it calculates the first term at the RHS
   Second, evaluates the sum over all the K^2 space
     (second term at the RHS)
   Then, adds the forcing term to the first and second terms.
   If flag_poincare = 1, calculates the point intersection with
   the Poincare surface by using the Henon method.
@param[in] t time
@param[in] v Matrix of size MAX_KX*MAX_KY*2 representing the complex coefficients of the velocity in Fourier space.
@param[out] vdot Solved derivative.
@param[in] e The unitary e vectors.
@param[in] f Matrix with forcing on each Fourier mode
@param[in] init_x Initial position of parallelized matrix v in the X direction
@param[in] end_x End position of parallelized matrix v in the X direction
@param[in] init_y Initial position of parallelized v matrix in the Y direction
@param[in] end_y End position of parallelized matrix v in the Y direction
@param[in] flag_poincare 1 to compute the Poincar\'e hypersurface of section, 0 for normal integration
@param[in] K_param Auxiliary variable to compute the Poincar\'e hypersurface of section
*/
void derivs2(double t, double v[MAX_KX][MAX_KY][2],
    double vdot[MAX_KX][MAX_KY][2], double e[MAX_KX][MAX_KY][2],
    double f[MAX_KX][MAX_KY][2], int init_x, int end_x, 
    int init_y, int end_y, 
    int flag_poincare, double K_param)
{
  int i, j, m, n, r, s, u, w;
  int k1, k2, p1, p2, q1, q2, t1, t2;

  double term1_re, term1_im;
  double sum_re, sum_im, temp1_Re, temp1_Im, temp2_Re, temp2_Im;

  double scalar1, scalar2, scalar3;

  double K;

  // Henon method for compute the Poincare surface of section
  // See Henon, M. Physica D, 5 (2-3), pp. 412-414
  K = 1./K_param;
  vdot[MAX_KX-1][MAX_KY-1][0] = K;

  for(i = init_x; i < end_x; i++)
  {
    for(j = init_y; j < end_y; j++)
    {
      // Obtain the k vector from the position in the matrix
      k1 = i;
      k2 = j - MAX_KY/2;

      sum_re = 0.;
      sum_im = 0.;

      if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
      {
        // First term of eq. (16) and (17)
        term1_re = 1./Re * (k1*k1 + k2*k2) * v[i][j][0];
        term1_im = 1./Re * (k1*k1 + k2*k2) * v[i][j][1];

        // The sumation over all K^2 space
        for(m = 0; m < MAX_KX; m++)
        {
          for(n = 0; n < MAX_KY; n++)
          {
            // Obtain the p vector from the position in the matrix
            p1 = m;
            p2 = n - MAX_KY/2;

            // Obtain the q = k - p vector
            bracket(&q1, &q2, k1 - p1, k2 - p2);

            // Obtain the t = k + p vector
            bracket(&t1, &t2, k1 + p1, k2 + p2);

            // Test if the p vector is in the K^2 subspace,
            // if it's in the truncation,
            // and if the q and t vectors are in the truncation
            if(is_in_subspace(p1, p2) && is_in_truncation(p1, p2)
	       && (p1 != k1 || p2 != k2) )
            {

              // Evaluate (e_p * k)
              scalar1 = e[m][n][0]*k1 + e[m][n][1]*k2;

              // Get the matrix indexes for the q = k - p vector
              r = q1;
              s = q2 + MAX_KY/2;

	      if(is_in_truncation(q1, q2))
		{

              // Evaluate (e_<k-p> * e_k)
              scalar2 = e[r][s][0]*e[i][j][0] + e[r][s][1]*e[i][j][1];
	      temp1_Re = sgn(k1-p1, k2-p2)*v[m][n][0]*v[r][s][1] + v[m][n][1]*v[r][s][0];
	      temp1_Im = v[m][n][0]*v[r][s][0] - sgn(k1-p1, k2-p2)*v[m][n][1]*v[r][s][1];
		}
	      else
		{
		  scalar2 = 0.;
		  temp1_Re = 0.;
		  temp1_Im = 0.;
		}


              // Matrix indexes for the t = k + p vector
              u = t1;
              w = t2 + MAX_KY/2;

	      if(is_in_truncation(t1, t2))
		{

              // Evaluate (e_(k+p) * e_k)
              scalar3 = e[u][w][0]*e[i][j][0] + e[u][w][1]*e[i][j][1];
	      temp2_Re = v[m][n][0]*v[u][w][1] - v[m][n][1]*v[u][w][0];
	      temp2_Im = v[m][n][0]*v[u][w][0] + v[m][n][1]*v[u][w][1];
		}
	      else
		{
		  scalar3 = 0.;
		  temp2_Re = 0.;
		  temp2_Im = 0.;
		}

              // Evaluate the sum
              sum_re = sum_re + scalar1*(
                  scalar2*temp1_Re
                + scalar3*temp2_Re
                                        );

              sum_im = sum_im + scalar1*(
                  scalar2*temp1_Im
                + scalar3*temp2_Im
                                        );
            }
          }
        }

        // Finally, evaluate dv_k/dt
        vdot[i][j][0] = K*(-term1_re + sum_re + f[i][j][0]);
        vdot[i][j][1] = K*(-term1_im - sum_im + f[i][j][1]);

      } // Subspace for the k vector
    }
  }

}
/**
   Another version of the infamous derivs routine.
   It evaluates the equations (16) & (17)
   from Feudel & Seehafer (1995) in three parts:
   First, it calculates the first term at the RHS
   Second, evaluates the sum over all the K^2 space
     (second term at the RHS)
   Then, adds the forcing term to the first and second terms.
   If flag_poincare = 1, calculates the point intersection with
   the Poincare surface by using the Henon method.
@param[in] t time
@param[in] v Matrix of size MAX_KX*MAX_KY*2 representing the complex coefficients of the velocity in Fourier space.
@param[out] vdot Solved derivative.
@param[in] e The unitary e vectors.
@param[in] f Matrix with forcing on each Fourier mode
@param[in] init_x Initial position of parallelized matrix v in the X direction
@param[in] end_x End position of parallelized matrix v in the X direction
@param[in] init_y Initial position of parallelized v matrix in the Y direction
@param[in] end_y End position of parallelized matrix v in the Y direction
@param[in] flag_poincare 1 to compute the Poincar\'e hypersurface of section, 0 for normal integration
@param[in] K_param Auxiliary variable to compute the Poincar\'e hypersurface of section
*/
void derivs(double t, double v[MAX_KX][MAX_KY][2],
    double vdot[MAX_KX][MAX_KY][2], double e[MAX_KX][MAX_KY][2],
    double f[MAX_KX][MAX_KY][2], int init_x, int end_x, 
    int init_y, int end_y, 
    int flag_poincare, double K_param)
{
  int i, j, m, n, r, s, u, w;
  int k1, k2, p1, p2, q1, q2, t1, t2;

  double term1_re, term1_im;
  double sum_re, sum_im, temp1_Re, temp1_Im, temp2_Re, temp2_Im;

  double scalar1, scalar2, scalar3;

  double K;

  // Henon method for compute the Poincare surface of section
  // See Henon, M. Physica D, 5 (2-3), pp. 412-414
  K = 1./K_param;
  vdot[MAX_KX-1][MAX_KY-1][0] = K;

  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      if(  i*MAX_KY + j >= init_x*MAX_KY + init_y
        && i*MAX_KY + j <=  end_x*MAX_KY + end_y)
      {

      // Obtain the k vector from the position in the matrix
      k1 = i;
      k2 = j - MAX_KY/2;

      sum_re = 0.;
      sum_im = 0.;

      if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
      {
        // First term of eq. (16) and (17)
        term1_re = 1./Re * (k1*k1 + k2*k2) * v[i][j][0];
        term1_im = 1./Re * (k1*k1 + k2*k2) * v[i][j][1];

        // The sumation over all K^2 space
        for(m = 0; m < MAX_KX; m++)
        {
          for(n = 0; n < MAX_KY; n++)
          {
            // Obtain the p vector from the position in the matrix
            p1 = m;
            p2 = n - MAX_KY/2;

            // Obtain the q = k - p vector
            bracket(&q1, &q2, k1 - p1, k2 - p2);

            // Obtain the t = k + p vector
            bracket(&t1, &t2, k1 + p1, k2 + p2);

            // Test if the p vector is in the K^2 subspace,
            // if it's in the truncation,
            // and if the q and t vectors are in the truncation
            if(is_in_subspace(p1, p2) && is_in_truncation(p1, p2)
	       && (p1 != k1 || p2 != k2) )
            {

              // Evaluate (e_p * k)
              scalar1 = e[m][n][0]*k1 + e[m][n][1]*k2;

              // Get the matrix indexes for the q = k - p vector
              r = q1;
              s = q2 + MAX_KY/2;

	      if(is_in_truncation(q1, q2))
		{

              // Evaluate (e_<k-p> * e_k)
              scalar2 = e[r][s][0]*e[i][j][0] + e[r][s][1]*e[i][j][1];
	      temp1_Re = sgn(k1-p1, k2-p2)*v[m][n][0]*v[r][s][1] + v[m][n][1]*v[r][s][0];
	      temp1_Im = v[m][n][0]*v[r][s][0] - sgn(k1-p1, k2-p2)*v[m][n][1]*v[r][s][1];
		}
	      else
		{
		  scalar2 = 0.;
		  temp1_Re = 0.;
		  temp1_Im = 0.;
		}


              // Matrix indexes for the t = k + p vector
              u = t1;
              w = t2 + MAX_KY/2;

	      if(is_in_truncation(t1, t2))
		{

              // Evaluate (e_(k+p) * e_k)
              scalar3 = e[u][w][0]*e[i][j][0] + e[u][w][1]*e[i][j][1];
	      temp2_Re = v[m][n][0]*v[u][w][1] - v[m][n][1]*v[u][w][0];
	      temp2_Im = v[m][n][0]*v[u][w][0] + v[m][n][1]*v[u][w][1];
		}
	      else
		{
		  scalar3 = 0.;
		  temp2_Re = 0.;
		  temp2_Im = 0.;
		}

              // Evaluate the sum
              sum_re = sum_re + scalar1*(
                  scalar2*temp1_Re
                + scalar3*temp2_Re
                                        );

              sum_im = sum_im + scalar1*(
                  scalar2*temp1_Im
                + scalar3*temp2_Im
                                        );
            }
          }
        }

        // Finally, evaluate dv_k/dt
        vdot[i][j][0] = K*(-term1_re + sum_re + f[i][j][0]);
        vdot[i][j][1] = K*(-term1_im - sum_im + f[i][j][1]);

      } // Subspace for the k vector
      }
    }
  }

}
