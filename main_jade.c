#include "headers.h"
#include <stdlib.h>

double norm_div(double *vx, double *vy);

// The main function
main(int argc, char **argv)
{
  int i, j, l, m, n, o;
  int count;

// Tracers
//  double x0[NUM_TRACERS], y0[NUM_TRACERS];

  double tfinal, t;
  double t_previous;
  double e[MAX_KX][MAX_KY][2];
  double f[MAX_KX][MAX_KY][2];
  double v[MAX_KX][MAX_KY][2];
#ifdef CALCULATE_POINCARE_POINTS
  double v_previous[MAX_KX][MAX_KY][2];
#endif

/*
  double vort_fourier[MAX_KX][MAX_KY][2];
  double temp_vort_fourier[MAX_KY][MAX_KX][2];
  double vort[M][N];
*/

  double k_eta, eta, base, exponent, sum1;

  // MPI related variables
  int my_pid, N_pid;
  int init_x, end_x, init_y, end_y;
  MPI_Datatype *columns;

#ifdef WITH_FFTW
  double vx_fourier_space[MAX_KY_INTERP][MAX_KX_INTERP][2];
  double vy_fourier_space[MAX_KY_INTERP][MAX_KX_INTERP][2];
  fftw_plan p1, p2, p3;
#endif
#ifdef WITH_DFT
  double *vx_fourier_space, *vy_fourier_space;
#endif
  double *vx_real_space, *vy_real_space;

  FILE *fp1, *fp2, *fp3, *fp4, *fp5, *fp6;

  int k1, k2;

  // Initialize the MPI Library
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_pid);
  MPI_Comm_size(MPI_COMM_WORLD, &N_pid);

  // This kind of conditions to check whether the files are
  // properly open or not comes from my computing engineer 
  // formation, so they are kind of a "trauma".
  // Nowadays, the only reason a modern computer cannot open
  // a file for writing is because the hard disk is on fire.
  if(open_output_files(&fp1, &fp2, &fp3, &fp4, &fp5, my_pid))
  {
    if(my_pid == 0)
      fp6 = fopen("dissipation_number.dat", "w");

    // Distribute work between the processors
    columns = NULL;
    columns = (MPI_Datatype *) malloc(N_pid*sizeof(MPI_Datatype));
                                                                                
    split_matrix(N_pid, my_pid, MAX_KX, MAX_KY, &init_x, &end_x, &init_y, &end_y, columns);

//    init_y = 0;
//    end_y  = MAX_KY;

#ifdef WITH_TRACERS
    initialize_tracer_positions(x0, y0);
#endif

    initialize_e_vectors(e);

#ifdef SAVE_REAL_SPACE

    vx_real_space    = (double *)malloc(sizeof(double)*M*N);
    vy_real_space    = (double *)malloc(sizeof(double)*M*N);

#ifdef WITH_FFTW

    // Initialization of the fftw library
    p1 = fftw_plan_dft_c2r_2d(M, N, &vx_fourier_space[0],
        vx_real_space, FFTW_ESTIMATE);

    p2 = fftw_plan_dft_c2r_2d(M, N, &vy_fourier_space[0],
        vy_real_space, FFTW_ESTIMATE);

/*
    p3 = fftw_plan_dft_c2r_2d(M, N, &temp_vort_fourier[0][0],
        &vort[0][0], FFTW_ESTIMATE);
*/
#endif

#ifdef WITH_DFT
    // One fine day I will get rid of static matrices.
    // After that I will attain the imperturbable stillness of mind
    // and the fires of aversion, delusion and desire will be
    // finally extinguished from me. I will become one with Nirvana.
    // (Ok I took that text from wikipedia)
    vx_fourier_space = (double *)malloc(sizeof(double)*MAX_KX*MAX_KY*2);
    vy_fourier_space = (double *)malloc(sizeof(double)*MAX_KX*MAX_KY*2);

#endif
#endif // SAVE_REAL_SPACE

      initialize_control_parameter(f);
//      read_initial_conditions(v);
//      set_initial_conditions(v, e);
//      t = load(v);
      load(&t, v);
      printf("v[4][1] = %12.11lf\n", v[8][2+MAX_KY/2][0]);


      // Select two modes for plotting: (l, m) & (n, o)
      // (2, -2), (4, 1)
      l = 2;
      m = -2 + MAX_KY/2.;

      n = 4;
      o = 1 + MAX_KY/2;

//      v[MAX_KX-1][MAX_KY-1][0] = t;
      v[MAX_KX-1][MAX_KY-1][0] = 0.;
      t = 0.;


      // Throw transient values away
      for(count = 0; t < TRANSIENT_TIME; count++)
      {

        t = v[MAX_KX-1][MAX_KY-1][0];

        rk4_parallel(v, t, TRANSIENTS_STEP, e, f, N_pid, my_pid, init_x, end_x, init_y, end_y, 0, columns);

        // This is some sort of ``progress bar''
        if(count % 1000 == 0 && my_pid == 0)
        {
          printf("Transient: %lf\n", t);
        }
      } // Transients


#ifdef CALCULATE_POINCARE_POINTS
      // Initialize v_previous, necessary for
      // the computation of the Poincare section
      copy_matrices(v, v_previous);
#endif

      // Simulation (do not reset time (t) nor count variables)
      for(; t < MAX_TIME; count++)
//      for(; count < 1; count++)
      {

        rk4_parallel(v, t, STEP, e, f, N_pid, my_pid, init_x, end_x, init_y, end_y, 0, columns);
        t = v[MAX_KX-1][MAX_KY-1][0];

#ifdef CALCULATE_POINCARE_POINTS
        // Check if the Poincare surface of section was crossed
        compute_poincare_section(v, v_previous, e, f, &t, t_previous,
         N_pid, my_pid, init_x, end_x, init_y, end_y, columns, fp4);
        // Saves previous states (for the Poincare section routine)
        copy_matrices(v, v_previous);
//        t_previous = t;
#endif //CALCULATE_POINCARE_POINTS

#ifdef SAVE_FOURIER_SPACE
        // Processor ID 0 saves the outcomes
        if(my_pid == 0 && count % 10 == 0)
        {
          fwrite(&t, sizeof(double), 1, fp1);
          fwrite((void *)v, sizeof(double), MAX_KX*MAX_KY*2, fp1);
        }
#endif //SAVE_FOURIER_SPACE

        // Enstrophy dissipation number
        sum1 = 0.;
        for(l = 0; l < MAX_KX; l++)
        {
          for(m = 0; m < MAX_KY; m++)
          {
            k1 = l;
            k2 = m - MAX_KY/2;
                                                                                
            if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
            {
              sum1 =  sum1 + pow((k1*k1 + k2*k2), 2)*v[l][m][0]*v[l][m][0]
                           + pow((k1*k1 + k2*k2), 2)*v[l][m][1]*v[l][m][1];
            }
          }
        }

        eta = (1./Re)*sum1;

//      Grrrrr
        base = eta/((1./Re)*(1./Re)*(1./Re));
        exponent = 1./6.;
                                                                                
        k_eta = pow(base, exponent);
                                                                                
        if(my_pid == 0 && count%10 == 0)
        {
          fprintf(fp6, "Enstrophy dissipation number: %lf %lf\n", t, k_eta);
//          fprintf(stdout, "Enstrophy dissipation number: %lf %lf\n", t, k_eta);
          fflush(fp6);
        }

        if(!finite(k_eta))
        {
          fprintf(fp6, "Error with the enstrophy dissipation number!!!!\n");
          // Abort execution
          if(my_pid == 0)
          {
            fclose(fp1);
            fclose(fp2);
            fclose(fp3);
            fclose(fp4);
            fclose(fp5);
            fclose(fp6);
          }
          free(columns);
          MPI_Finalize();
          return 0;
        }

#ifdef SAVE_REAL_SPACE
        // Write the fluid state in real space

#ifdef WITH_FFTW
        // Initialize the matrices for the FFTW library
        fftshift(v, vx_fourier_space, e, 0);
        fftshift(v, vy_fourier_space, e, 1);

        // Calculate vorticity in Fourier space
/*
        for(i = 0; i < MAX_KX; i++)
        {
          for(j = 0; j < MAX_KY; j++)
          {
            k1 = i;
            k2 = j - MAX_KY/2;

            if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
            {
              vort_fourier[i][j][0] = -(k1*e[i][j][1] - k2*e[i][j][0])*v[i][j][1];
              vort_fourier[i][j][1] = (k1*e[i][j][1] - k2*e[i][j][0])*v[i][j][0];
            }
          }
        }

        fftshift_simple(vort_fourier, temp_vort_fourier);
*/

        // Calculate the inverse fft
        fftw_execute(p1);
        fftw_execute(p2);
//        fftw_execute(p3);

#endif //WITH_FFTW

#ifdef WITH_DFT
        for(i = 0; i < MAX_KX; i++)
        {
          for(j = 0; j < MAX_KY; j++)
          {
            vx_fourier_space[i*MAX_KY*2 + j*2 + 0] = v[i][j][0]*e[i][j][0];
            vx_fourier_space[i*MAX_KY*2 + j*2 + 1] = v[i][j][1]*e[i][j][0];

            vy_fourier_space[i*MAX_KY*2 + j*2 + 0] = v[i][j][0]*e[i][j][1];
            vy_fourier_space[i*MAX_KY*2 + j*2 + 1] = v[i][j][1]*e[i][j][1];
          }
        }

        dullft2(vx_fourier_space, vx_real_space);
        dullft2(vy_fourier_space, vy_real_space);

#endif //WITH_DFT

        // Write the results to a file
        if(my_pid == 0 && count % 10 == 0)
        {
/*
          // ASCII format
          for(i = 0; i < M; i++)
          {
            for(j = 0; j < N; j++)
            {
              fprintf(fp2, "%lf %lf %lf %lf\n",
                                       (2.*M_PI/M)*(double)i,
                                       (2.*M_PI/N)*(double)j,

#ifdef WITH_FFTW
                                       vx_real_space[j*N + i],
                                       vy_real_space[j*N + i]);
#endif
#ifdef WITH_DFT
                                       vx_real_space[i*N + j],
                                       vy_real_space[i*N + j]);
#endif
            }
          }
*/

          fwrite(&t, sizeof(double), 1, fp2);
          fwrite((void *)vx_real_space, sizeof(double), M*N, fp2);
          fwrite((void *)vy_real_space, sizeof(double), M*N, fp2);

//          fwrite(&t, sizeof(double), 1, fp5);
//          fwrite((void *)vort, sizeof(double), M*N, fp5);
        }


        // Compute the divergence
        if(count % 100 == 0 && my_pid == 0)
        {
          fprintf(stderr, "Norm of divergence: t = %lf, ||DIV|| = %20.19lf\n",
                                  t, norm_div(vx_real_space, vy_real_space));
        }

#ifdef WITH_TRACERS
        for(i = 0; i < NUM_TRACERS; i++)
        {
          // Save tracer position
          x0[i] += vx_real_space[(int)x0[i]][(int)y0[i]]*STEP;
          y0[i] += vy_real_space[(int)x0[i]][(int)y0[i]]*STEP;
          if(x0[i] > M)
            x0[i] = x0[i] - (double)M;
          else if(x0[i] < 0.)
            x0[i] = x0[i] + (double)M;

          if(y0[i] > N)
            y0[i] = y0[i] - (double)N;
          else if(y0[i] < 0.)
            y0[i] = y0[i] + (double)N;

          fprintf(fp3, "%lf %lf %lf %lf\n", x0[i], y0[i]-MAX_KY/2, 0.2, 0.2);
        }
#endif // WITH_TRACERS
#endif // SAVE_REAL_SPACE

        // Show some kind of "progress bar"
        if(count % 100 == 0 && my_pid == 0)
        {
          printf("%lf %lf %lf %lf %lf\n", t, v[l][m][0], v[l][m][1], v[n][o][0], v[n][o][1]);
        }

      } // Simulation

    if(my_pid == 0)
    {
      fclose(fp1);
      fclose(fp2);
      fclose(fp3);
      fclose(fp4);
      fclose(fp5);
    }
    free(columns);

    free(vx_real_space);
    free(vy_real_space);
    vx_real_space = NULL;
    vy_real_space = NULL;

#ifdef WITH_DFT
    free(vx_fourier_space);
    free(vy_fourier_space);
    vx_fourier_space = NULL;
    vy_fourier_space = NULL;
#endif

  }

  MPI_Finalize();
  return 0;
}
