#include "headers.h"

#if 0

/** 
Debug function (shows the contents of a matrix)

@param[in] matrix Matrix MxN to be visualized.
*/
void show_big_matrix(double matrix[M][N])
{
  int i, j;

  for(i = 0; i < M; i++)
  {
    for(j = 0; j < N; j++)
    {
      printf("(%d, %d) = %lf ", i, j, matrix[i][j]);
    }
    printf("\n");
  }
  printf("------\n");
}

/** 
Debug function (shows the contents of a complex matrix)

@param[in] matrix Complex matrix to be visualized (size MxNx2).
*/
void show_big_complex_matrix(double matrix[M][N][2])
{
  int i, j;

  for(i = 0; i < M; i++)
  {
    for(j = 0; j < N; j++)
    {
      printf("(%d, %d) = %lf, %lf\n", i, j, matrix[i][j][0], matrix[i][j][1]);
    }
    printf("\n");
  }
  printf("------\n");
}

/**
Debug function (shows the contents of a complex matrix)

@param[in] matrix Complex matrix to be displayed (size MAX_KX*MAX_KY*2)
*/
void show_matrix(double matrix[MAX_KX][MAX_KY][2])
{
  int i, j, k1, k2;

  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      k1 = i;
      k2 = j - MAX_KY/2;

      printf("(%d, %d)... (%d, %d): %lf %lf\n", i, j, k1, k2, matrix[i][j][0], matrix[i][j][1]);
    }
  }
}

/**
Debug function (shows the contents of a fft_shifted matrix)

@param[in] matrix FFT-shifted matrix to be displayed
*/
void show_fftshifted_matrix(double matrix[MAX_KY][MAX_KX][2])
{
  int i, j, k1, k2;

    for(j = 0; j < MAX_KX; j++)
  {
  for(i = 0; i < MAX_KY; i++)
    {
//      k1 = i - MAX_KY/2;
      k2 = i < MAX_KY/2 ? i : i - MAX_KY + 1;
      k1 = j;

      printf("(%d, %d)... (%d, %d): %lf %lf\n", i, j, k1, k2, matrix[i][j][0], matrix[i][j][1]);
    }
  }
}

#endif
