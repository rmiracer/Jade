#include <mpi.h>
#include <math.h>

/**
  Synchronize a matrix among parallel processes
@param[in] m Size of matrix
@param[in] n Size of matrix
@param[in] N_pid Total of parallel processes
@param[in] my_pid ID of current process
@param[in,out] Matrix to be synchronized
@param[in] columns  Parallelized columns of matrix v.
*/
void synchronize(int m, int n, int N_pid, int my_pid, double matrix[m][n][2], MPI_Datatype *columns1)
{
  int pid, dest;
  int init_interval, end_interval, increment;
  int i, j, k;

  MPI_Status status;

  MPI_Datatype columns[N_pid];

  for(pid = 0; pid < N_pid; pid++)
  {
    columns[pid] = columns1[pid];
  }

  for(pid = 0; pid < N_pid; pid++)
  {

      if(pid < my_pid)
      {
        MPI_Recv(matrix, 1, columns[pid], pid, 0, MPI_COMM_WORLD, &status);
      }
      else if(pid == my_pid)
      {
        for(dest = 0; dest < N_pid; dest++)
	{
	  if(dest != my_pid)
	  {
            MPI_Send(matrix, 1, columns[pid], dest, 0, MPI_COMM_WORLD);
	  }
	}
      }
      else if(pid > my_pid)
      {
        MPI_Recv(matrix, 1, columns[pid], pid, 0, MPI_COMM_WORLD, &status);
      }
  }
}

/**
  Display contents of a matrix.
@param[in] m Size of matrix
@param[in] n Size of matrix
@param[in] matrix Matrix to be displayed
*/
void show_matrix_contents(int m, int n, double matrix[m][n][2])
{
  int i, j;

  for(i = 0; i < m; i++)
    {
      for(j = 0; j < n; j++)
      {
        printf("(%lf %lf) ", matrix[i][j][0], matrix[i][j][1]);
      }
      printf("\n");
    }
}

/**
  Distributes elements of a matrix across processes
@param[in] N_pid Total of parallel processes
@param[in] my_pid ID of current process
@param[in] m Size of matrix
@param[in] n Size of matrix
@param[out] init_x  Initial position of parallelized matrix v in the X direction
@param[out] end_x  End position of parallelized matrix v in the X direction
@param[out] init_y  Initial position of parallelized v matrix in the Y direction
@param[out] end_y  End position of parallelized matrix v in the Y direction
@param[out] columns  Parallelized columns of matrix v.
*/
void split_matrix(int N_pid, int my_pid, int m, int n, int *x_index_init, int *x_index_end, int *y_index_init, int *y_index_end, MPI_Datatype *columns1)
{
  int i, j;
  int pid_count, elems_count;
  int block_location, block_length;
  int n_elems_matrix, n_elems_for_pid;
  MPI_Datatype two_doubles;
  MPI_Datatype columns[N_pid];

  n_elems_matrix = n*m;

  n_elems_for_pid = n_elems_matrix/N_pid;

  MPI_Type_vector(1, 2, 0, MPI_DOUBLE, &two_doubles);

  // Distributes matrix elements
  pid_count = 0.;
  elems_count = 0.;

  if(my_pid == 1000)
  {
    printf("Each pid will have %d elements\n", n_elems_for_pid);
  }

  for(i = 0; i < m; i++)
  {
    for(j = 0; j < n; j++)
    {
      if(my_pid == 1000)
      printf("%d %d\n", elems_count, pid_count);
      if(elems_count == 0)
      {
        if(pid_count == my_pid)
        {
          *x_index_init = i;
          *y_index_init = j;
//          printf("Element (%d, %d) assigned to PID %d\n", i, j, my_pid);
        }
        block_location = n*i+j;

        if(pid_count == N_pid - 1)
        {
          block_length = n_elems_matrix - n_elems_for_pid*(N_pid - 1);
        }
        else
        {
          block_length = n_elems_for_pid;
        }

        MPI_Type_indexed(1, &block_length, &block_location,
                         two_doubles, &(columns[pid_count]));
        MPI_Type_commit(&(columns[pid_count]));
      }
      elems_count++;
      if(elems_count == n_elems_for_pid)
      {
        elems_count = 0;
        if(pid_count == my_pid && pid_count < N_pid - 1)
        {
          *x_index_end = i;
          *y_index_end = j;
//          printf("Element (%d, %d) finish PID %d\n", i, j, my_pid);
        }
        pid_count++;
      }
    }
  }
  if(my_pid == N_pid - 1)
  {
    *x_index_end = m-1;
    *y_index_end = n-1;
//    printf("Element (%d, %d) finish PID %d\n", *x_index_end, *y_index_end, my_pid);
  }

  for(pid_count = 0; pid_count < N_pid; pid_count++)
  {
    columns1[pid_count] = columns[pid_count];
  }

}

#if 0
void split_matrix2(int N_pid, int my_pid, int m, int n, int *x_index_init, int *x_index_end, int *y_index_init, int *y_index_end, MPI_Datatype columns[N_pid])
{
  int pid_count;
  int block_location, block_length;

  MPI_Datatype two_doubles;

  *x_index_init = (int)((my_pid*m)/N_pid);
  if(my_pid + 1 != N_pid)
  {
    *x_index_end = (int)(ceil(((double)((my_pid + 1)*m)/(double)N_pid)));
  }
  else //Assign the remaining indexes to the last pid
  {
    *x_index_end = m;
  }

  if(N_pid > m)
  {
    *x_index_end = *x_index_init + 1;
    *y_index_init = (int)(my_pid*m*n/N_pid)%n;
//    *y_index_init = (int)((my_pid%(n/(N_pid/m)))*n/(N_pid/m));
    if(my_pid + 1 != N_pid)
    {
//      *y_index_end = (int)(((my_pid)%(n/(N_pid/m))+1)*n/(N_pid/m));
      *y_index_end = (int)(my_pid*m*n/N_pid)%n + m*n/N_pid;
    }
    else
    {
      *y_index_end = n;
    }
  }
  else
  {
    *y_index_init = 0;
    *y_index_end = n;
  }

  MPI_Type_vector(1, 2, 0, MPI_DOUBLE, &two_doubles);

  // Calculates the location and length of each portion
  // of the matrix for each processor
  for(pid_count = 0; pid_count < N_pid; pid_count++)
  {
    block_location = n*pid_count*(int)(m/N_pid);
    if(pid_count + 1 != N_pid)
    {
      block_length = n*(pid_count + 1)*(int)(m/N_pid) - block_location;
    }
    else
    {
      block_length = n*m - block_location;
    }

    MPI_Type_indexed(1, &block_length, &block_location, two_doubles, &(columns[pid_count]));
    MPI_Type_commit(&(columns[pid_count]));
  }
      
}
#endif
