#include "headers.h"

#ifdef WITH_DFT
int is_in_subspace(int k1, int k2);
int is_in_truncation(int k1, int k2);

void dullft2(double *in, double *out)
{
  int i, j, l, m;
  double x, y, k1, k2;
  double dx, dy, sum;

  dx = 2.*M_PI/(M);
  dy = 2.*M_PI/(N);

  for(i = 0; i < M; i++)
  {
    for(j = 0; j < N; j++)
    {
      x = dx*(double)i;
      y = dy*(double)j;

      sum = 0.;

      for(l = 0; l < MAX_KX; l++)
      {
        for(m = 0; m < MAX_KY; m++)
        {

          k1 = l;
          k2 = m - MAX_KY/2;

          if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
          {

            sum = sum + in[l*MAX_KY*2 + m*2 + 0]*cos(k1*x + k2*y)
                      - in[l*MAX_KY*2 + m*2 + 1]*sin(k1*x + k2*y);
          }
        }
      }

      out[i*N + j] = 2.*sum;
    }
  }
}

#endif //WITH_DFT
