#include "headers.h"

#define Lx (2.*M_PI)
#define Ly (2.*M_PI)

#ifdef WITH_FFTW
double y_diff(double *matrix, int i, int j)
{
  double dy, diff;

  dy = Ly/N;

  diff = (1./(12.*dy))*matrix[( ((j - 2)+M)%M )*N + i]
       - (2./( 3.*dy))*matrix[( ((j - 1)+M)%M )*N + i]
       + (2./( 3.*dy))*matrix[( ((j + 1)+M)%M )*N + i]
       - (1./(12.*dy))*matrix[( ((j + 2)+M)%M )*N + i];

  return diff;
}

double x_diff(double *matrix, int i, int j)
{
  double dx, diff;

  dx = Lx/N;

  diff = (1./(12.*dx))*matrix[j*N + ( ((i - 2)+N)%N )]
       - (2./( 3.*dx))*matrix[j*N + ( ((i - 1)+N)%N )]
       + (2./( 3.*dx))*matrix[j*N + ( ((i + 1)+N)%N )]
       - (1./(12.*dx))*matrix[j*N + ( ((i + 2)+N)%N )];

  return diff;
}
#endif

#ifdef WITH_DFT
double x_diff(double *matrix, int i, int j)
{
  double dx, diff;

  dx = Lx/M;

  diff = (1./(12.*dx))*matrix[( ((i - 2)+M)%M )*N + j]
       - (2./( 3.*dx))*matrix[( ((i - 1)+M)%M )*N + j]
       + (2./( 3.*dx))*matrix[( ((i + 1)+M)%M )*N + j]
       - (1./(12.*dx))*matrix[( ((i + 2)+M)%M )*N + j];

  return diff;
}

double y_diff(double *matrix, int i, int j)
{
  double dy, diff;

  dy = Ly/N;

  diff = (1./(12.*dy))*matrix[i*N + ( ((j - 2)+N)%N )]
       - (2./( 3.*dy))*matrix[i*N + ( ((j - 1)+N)%N )]
       + (2./( 3.*dy))*matrix[i*N + ( ((j + 1)+N)%N )]
       - (1./(12.*dy))*matrix[i*N + ( ((j + 2)+N)%N )];

  return diff;
}
#endif

double norm_div(double *vx, double *vy)
{
  int i, j;
  double div, sum;

  sum = 0.;
  for(i = 0; i < M; i++)
  {
    for(j = 0; j < N; j++)
    {
      div = x_diff(vx, i, j) + y_diff(vy, i, j);
      sum = sum + div*div;
    }
  }

  return (sum/((M - 1)*(N - 1)));
}
