#include "headers.h"

#include <string.h>

/**
   4th order 2D MPI-Paralelized Complex Runge-kutta
@param[in,out] v  Matrix of size MAX_KX*MAX_KY*2 representing the complex coefficients of the velocity in Fourier space.
@param[in, out] t  time
@param[in] h  Step size
@param[in] N_pid  number of parallel processes
@param[in] my_pid  ID of current parallel process
@param[in] init_x  Initial position of parallelized matrix v in the X direction
@param[in] end_x  End position of parallelized matrix v in the X direction
@param[in] init_y  Initial position of parallelized v matrix in the Y direction
@param[in] end_y  End position of parallelized matrix v in the Y direction
@param[in] flag_poincare  1 to compute the Poincar\'e hypersurface of section, 0 for normal integration
@param[in] columns  Parallelized columns of matrix v.
*/
void rk4_parallel(double v[MAX_KX][MAX_KY][2], double t, double h,
    double e[MAX_KX][MAX_KY][2], double f[MAX_KX][MAX_KY][2],
    int N_pid, int my_pid, int init_x, int end_x, 
    int init_y, int end_y,
    int flag_poincare, MPI_Datatype columns[N_pid])
{
  int i, j, l;
  double th, hh, h6;
  double dym[MAX_KX][MAX_KY][2], dyt[MAX_KX][MAX_KY][2],
         yt[MAX_KX][MAX_KY][2];
  double dydx[MAX_KX][MAX_KY][2], vout[MAX_KX][MAX_KY][2];
  double temp_matrix[MAX_KX][MAX_KY][2], temp;
                                                                                
  hh = h*0.5;
  h6 = h/6.0;
  th = t + hh;


  if(flag_poincare == 1)
  {
//    derivs(t, v, dydx, e, f, 4, 5, 1+MAX_KY/2, 2+MAX_KY/2, 0);
    derivs2(t, v, temp_matrix, e, f, 8, 9, 2+MAX_KY/2, 3+MAX_KY/2, 0, 1.);
    temp = temp_matrix[8][2+MAX_KY/2][0];
  }
  else
  {
    temp = 1.;
  }

  // First step
  derivs(t, v, dydx, e, f, init_x, end_x, init_y, end_y, flag_poincare, temp);
                                                                                
  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      if(  i*MAX_KY + j >= init_x*MAX_KY + init_y
        && i*MAX_KY + j <=  end_x*MAX_KY + end_y)
      {
        for(l = 0; l < 2; l++)
        {
          yt[i][j][l] = v[i][j][l] + hh*dydx[i][j][l];
        }
      }
    }
  }
                                                                                
#ifdef MPI_DEBUG
  printf("synchronizing 0\n");
#endif
                                                                                
  synchronize(MAX_KX, MAX_KY, N_pid, my_pid, yt, columns);
                                                                                
  if(flag_poincare == 1)
  {
//    derivs(th, yt, dyt, e, f, 4, 5, 1+MAX_KY/2, 2+MAX_KY/2, 0);
    derivs2(th, yt, temp_matrix, e, f, 8, 9, 2+MAX_KY/2, 3+MAX_KY/2, 0, 1.);
    temp = temp_matrix[8][2+MAX_KY/2][0];
  }
  else
  {
    temp = 1.;
  }

  // Second step
  derivs(th, yt, dyt, e, f, init_x, end_x, init_y, end_y, flag_poincare, temp);

  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      if(  i*MAX_KY + j >= init_x*MAX_KY + init_y
        && i*MAX_KY + j <=  end_x*MAX_KY + end_y)
      {
        for(l = 0; l < 2; l++)
        {
          yt[i][j][l] = v[i][j][l] + hh*dyt[i][j][l];
        }
      }
    }
  }
                                                                                
#ifdef MPI_DEBUG
  printf("synchronizing 1\n");
#endif
                                                                                
  synchronize(MAX_KX, MAX_KY, N_pid, my_pid, yt, columns);

  if(flag_poincare == 1)
  {
//    derivs(th, yt, dym, e, f, 4, 5, 1+MAX_KY/2, 2+MAX_KY/2, 0);
    derivs2(th, yt, temp_matrix, e, f, 8, 9, 2+MAX_KY/2, 3+MAX_KY/2, 0, 1.);
    temp = temp_matrix[8][2+MAX_KY/2][0];
  }
  else
  {
    temp = 1.;
  }

  // Third step
  derivs(th, yt, dym, e, f, init_x, end_x, init_y, end_y, flag_poincare, temp);

  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      if(  i*MAX_KY + j >= init_x*MAX_KY + init_y
        && i*MAX_KY + j <=  end_x*MAX_KY + end_y)
      {
        for(l = 0; l < 2; l++)
        {
          yt[i][j][l] = v[i][j][l] + h*dym[i][j][l];
          dym[i][j][l] += dyt[i][j][l];
        }
      }
    }
  }
                                                                                
#ifdef MPI_DEBUG
  printf("synchronizing 2\n");
#endif
                                                                                
  synchronize(MAX_KX, MAX_KY, N_pid, my_pid, yt, columns);
                                                                                
  if(flag_poincare == 1)
  {
//    derivs(t+h, yt, dyt, e, f, 4, 5, 1+MAX_KY/2, 2+MAX_KY/2, 0);
    derivs2(t+h, yt, temp_matrix, e, f, 8, 9, 2+MAX_KY/2, 3+MAX_KY/2, 0, 1.);
    temp = temp_matrix[8][2+MAX_KY/2][0];
  }
  else
  {
    temp = 1.;
  }


  // Fourth step
  derivs(t+h, yt, dyt, e, f, init_x, end_x, init_y, end_y, flag_poincare, temp);

  // Accumulate increments with proper weights
  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      if(  i*MAX_KY + j >= init_x*MAX_KY + init_y
        && i*MAX_KY + j <=  end_x*MAX_KY + end_y)
      {
        for(l = 0; l < 2; l++)
        {
          vout[i][j][l] = v[i][j][l] + h6*(dydx[i][j][l] + dyt[i][j][l]
              + 2.0*dym[i][j][l]);
          v[i][j][l] = vout[i][j][l];
        }
      }
    }
  }
                                                                                
#ifdef MPI_DEBUG
  printf("synchronizing 3\n");
#endif
                                                                                
  synchronize(MAX_KX, MAX_KY, N_pid, my_pid, v, columns);

}

#if 0
/**
   Initialize the matrix for the FFT library
@param[in] v  Matrix of size MAX_KX*MAX_KY*2 representing the complex coefficients of the velocity in Fourier space.
@param[out] matrix_fft  Matrix for FFTW.
*/
void set_matrix(double v[MAX_KX][MAX_KY][2], double matrix_fft[M][N][2])
{
  int i, j, l, m;
  int k1, k2;
  double K_space[M][N][2];

  // The K_space matrix has the (0, 0) point
  // at the middle of the matrix
  for(l = M/2, i = 0; l >= 0; l--, i++)
  {
    for(m = N - 1, j = 0; m >= 0; m--, j++)
    {
      if(!(l == M/2 && m >= N/2))
      {
        k1 = i;
        k2 = j - MAX_KY/2;

        if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
        {
          K_space[l][m][0] = v[i][j][0];
          K_space[l][m][1] = -v[i][j][1];
          K_space[M-l-1][N-m-1][0] = v[i][j][0];
          K_space[M-l-1][N-m-1][1] = v[i][j][1];
        }
        else
        {
          K_space[l][m][0] = 0.;
          K_space[l][m][1] = 0.;
          K_space[M-l-1][N-m-1][0] = 0.;
          K_space[M-l-1][N-m-1][1] = 0.;
        }
      }
    }
  }

  // Rearrange the result in the output matrix
  for(l = 0; l < M; l++)
  {
    for(m = 0; m < N; m++)
    {
      matrix_fft[l][m][0] = K_space[(l+M/2)%M][(m+N/2)%N][0];
      matrix_fft[l][m][1] = K_space[(l+M/2)%M][(m+N/2)%N][1];
    }
  }

}

/**
   Initialize the matrix for the FFT library
   In this case the values of the output matrix are multiplied
   by one of the components of the unitary e vectors

@param[in] v  Matrix of size MAX_KX*MAX_KY*2 representing the complex coefficients of the velocity in Fourier space.
@param[out] matrix_fft  Matrix for FFTW.
@param[in] e  The unitary e vectors.
@param[in] dir  Direction of e vector.
*/
void set_matrix_2D(double v[MAX_KX][MAX_KY][2], double matrix_fft[M][N][2], double e[MAX_KX][MAX_KY][2], int dir)
{
  int i, j, l, m;
  int k1, k2;
  double K_space[M][N][2];

  // Be sure that the K_space array is ``clean''
  // I don't know why, but I had problems with this
  memset(K_space, 0, M*N*2*sizeof(double));

    for(l = M/2, i = 0; l >= 0; l--, i++)
    {
      for(m = N - 1, j = 0; m >= 0; m--, j++)
      {
        if(!(l == M/2 && m >= N/2))
        {
          k1 = i;
          k2 = j - MAX_KY/2;

          if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
          {
            K_space[l][m][0] = v[i][j][0]*e[i][j][dir];
            K_space[l][m][1] = -v[i][j][1]*e[i][j][dir];
            K_space[M-l-1][N-m-1][0] = v[i][j][0]*e[i][j][dir];
            K_space[M-l-1][N-m-1][1] = v[i][j][1]*e[i][j][dir];
          }
          else
          {
            K_space[l][m][0] = 0.;
            K_space[l][m][1] = 0.;
            K_space[M-l-1][N-m-1][0] = 0.;
            K_space[M-l-1][N-m-1][1] = 0.;
          }
        }
      }
    }

  for(l = 0; l < M; l++)
  {
    for(m = 0; m < N; m++)
    {
      matrix_fft[l][m][0] = K_space[(l+M/2)%M][(m+N/2)%N][0];
      matrix_fft[l][m][1] = K_space[(l+M/2)%M][(m+N/2)%N][1];
    }
  }

}
#endif

/**
   Perform matrix shifting for the ifft routine
@param[in] v  Matrix of size MAX_KX*MAX_KY*2 representing the complex coefficients of the velocity in Fourier space.
@param[out] v_shifted  The shifted matrix
@param[in] e  The unitary e vectors.
@param[in] dir  Direction of e vector.
*/
void fftshift(double v[MAX_KX][MAX_KY][2], double v_shifted[MAX_KY_INTERP][MAX_KX_INTERP][2],
    double e[MAX_KX][MAX_KY][2], int dir)
{
  int i, j, k;
  int k1, k2;

  memset(v_shifted, 0, MAX_KY_INTERP*MAX_KX_INTERP*2*sizeof(double));
                                                                                
  // MAX_KY is odd
  if(MAX_KY % 2 == 1)
  {
    for(i = 0; i < MAX_KX; i++)
    {
      for(j = 0; j < MAX_KY; j++)
      {
        k1 = i;
        k2 = j - MAX_KY/2;
                                                                                
        if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
        {
          for(k = 0; k < 2; k++)
          {
            v_shifted[(MAX_KY_INTERP - MAX_KY/2 + j)%MAX_KY_INTERP][i][k] = v[i][j][k]*e[i][j][dir];
          }
        }
        else
        {
          for(k = 0; k < 2; k++)
          {
            v_shifted[(MAX_KY_INTERP - MAX_KY/2 + j)%MAX_KY_INTERP][i][k] = 0.;
          }
        }
      }
    }

    for(i = 1.; i < MAX_KY_INTERP/2; i++)
    {
      v_shifted[MAX_KY_INTERP-i][0][0] = v_shifted[i][0][0];
      v_shifted[MAX_KY_INTERP-i][0][1] = -v_shifted[i][0][1];
    }

    v_shifted[0][0][0] = 0.;
    v_shifted[0][0][1] = 0.;
  }
  else // MAX_KY is even
  {
    for(i = 0; i < MAX_KX; i++)
    {
      for(j = 0; j < MAX_KY/2; j++)
      {
        for(k = 0; k < 2; k++)
        {
          v_shifted[MAX_KY/2 - j - 1][i][k] = v[i][j][k]*e[i][j][dir];
          v_shifted[MAX_KY - j - 1][i][k]
            = v[i][MAX_KY/2 + j][k]*e[i][MAX_KY/2 + j][dir];
        }
      }
    }
  }
}

#if 0
void fftshift_old(double v[MAX_KX][MAX_KY][2], double v_shifted[MAX_KY][MAX_KX][2],
    double e[MAX_KX][MAX_KY][2], int dir)
{
  int i, j, k;
  int k1, k2;
                                                                                
  memset(v_shifted, 0, MAX_KY*MAX_KX*2*sizeof(double));
                                                                                
  // MAX_KY is odd
  if(MAX_KY % 2 == 1)
  {
    for(i = 0; i < MAX_KX; i++)
    {
      for(j = 0; j < MAX_KY; j++)
      {
        k1 = i;
        k2 = j - MAX_KY/2;
                                                                                
        if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
        {
          for(k = 0; k < 2; k++)
          {
            v_shifted[(MAX_KY - MAX_KY/2 + j)%MAX_KY][i][k] = v[i][j][k]*e[i][j][dir];
          }
        }
      }
    }

    for(i = 0.; i < MAX_KY/2; i++)
    {
      for(j = 0.; j < MAX_KX; j++)
      {
        if(i >= 1 && j == 0)
        {
          v_shifted[MAX_KY-i][0][0] = v_shifted[i][j][0];
          v_shifted[MAX_KY-i][0][1] = -v_shifted[i][j][1];
        }
      }
    }

    v_shifted[0][0][0] = 0.;
    v_shifted[0][0][1] = 0.;
  }
  else // MAX_KY is even
  {
    for(i = 0; i < MAX_KX; i++)
    {
      for(j = 0; j < MAX_KY/2; j++)
      {
        for(k = 0; k < 2; k++)
        {
          v_shifted[MAX_KY/2 - j - 1][i][k] = v[i][j][k]*e[i][j][dir];
          v_shifted[MAX_KY - j - 1][i][k]
            = v[i][MAX_KY/2 + j][k]*e[i][MAX_KY/2 + j][dir];
        }
      }
    }
  }
}
#endif

/**
   Perform matrix shifting for the ifft routine
@param[in] v  Matrix of size MAX_KX*MAX_KY*2 representing the complex coefficients of the velocity in Fourier space.
@param[out] v_shifted  The shifted matrix
*/
void fftshift_simple(double v[MAX_KX][MAX_KY][2], double v_shifted[MAX_KY][MAX_KX][2])
{
  int i, j, k;
  int k1, k2;
                                                                                
  memset(v_shifted, 0, MAX_KY*MAX_KX*2*sizeof(double));
                                                                                
  // MAX_KY is odd
  if(MAX_KY % 2 == 1)
  {
    for(i = 0; i < MAX_KX; i++)
    {
      for(j = 0; j < MAX_KY; j++)
      {
        k1 = i;
        k2 = j - MAX_KY/2;
                                                                                
        if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
        {
          for(k = 0; k < 2; k++)
          {
//            v_shifted[(MAX_KY/2 + 1 + j)%MAX_KY][i][k] = v[i][j][k];
            v_shifted[(MAX_KY - MAX_KY/2 + j)%MAX_KY][i][k] = v[i][j][k];
          }
        }
	else
        {
          for(k = 0; k < 2; k++)
          {
            v_shifted[(MAX_KY - MAX_KY/2 + j)%MAX_KY][i][k] = 0.;
          }
        }
      }
    }
    for(i = 0.; i < MAX_KY/2; i++)
    {
      for(j = 0.; j < MAX_KX; j++)
      {
//          vx_fourier_space[i][j][0] = 1.;
//          vx_fourier_space[i][j][1] = 1.;
        if(i >= 1 && j == 0)
        {
          v_shifted[MAX_KY-i][0][0] = v_shifted[i][j][0];
          v_shifted[MAX_KY-i][0][1] = -v_shifted[i][j][1];
        }
      }
    }

    v_shifted[0][0][0] = 0.;
    v_shifted[0][0][1] = 0.;

  }
  else // MAX_KY is even
  {
    for(i = 0; i < MAX_KX; i++)
    {
      for(j = 0; j < MAX_KY/2; j++)
      {
        for(k = 0; k < 2; k++)
        {
          v_shifted[MAX_KY/2 - j - 1][i][k] = v[i][j][k];
          v_shifted[MAX_KY - j - 1][i][k]
            = v[i][MAX_KY/2 + j][k];
        }
      }
    }
  }
}

#if 0
/**
   Perform matrix shifting for the ifft routine (test routine)
@param[n] v  Matrix of size MAX_KX*MAX_KY*2 representing the complex coefficients of the velocity in Fourier space.
@param[out] v_shifted  The shifted matrix
*/
void fftshift_test(double v[MAX_KX][MAX_KY][2], double v_shifted[MAX_KY][MAX_KX][2],
    double e[MAX_KX][MAX_KY][2], int dir)
{
  int i, j, k, l;
  int k1, k2;

  memset(v_shifted, 0, MAX_KY*MAX_KX*2*sizeof(double));

  // MAX_KY is odd
  if(MAX_KY % 2 == 1)
  {
    for(i = 0; i < MAX_KX; i++)
    {
//      for(j = 0; j < MAX_KY/2; j++)
      for(j = 0; j < MAX_KY; j++)
      {
        k1 = i;
        k2 = j - MAX_KY/2;

        if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
        {
          for(k = 0; k < 2; k++)
          {
            v_shifted[(MAX_KY/2 + 1 + j)%MAX_KY][i][k] = v[i][j][k];
/*
            v_shifted[MAX_KY/2 - j][i][k] = v[i][l][k];
            v_shifted[MAX_KY - j - 1][i][k] 
              = v[i][MAX_KY/2 + j + 1][k];
*/
          }
        }
      }
/*
      for(k = 0; k < 2; k++)
      {
        if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
        {
          l = MAX_KY/2 - j
          v_shifted[MAX_KY/2 - j][i][k] = v[i][j][k];
        }
      }
*/
    }
  }
  else // MAX_KY is even
  {
    for(i = 0; i < MAX_KX; i++)
    {
      for(j = 0; j < MAX_KY/2; j++)
      {
        for(k = 0; k < 2; k++)
        {
          v_shifted[MAX_KY/2 - j - 1][i][k] = v[i][j][k]*e[i][j][dir];
          v_shifted[MAX_KY - j - 1][i][k] 
            = v[i][MAX_KY/2 + j][k]*e[i][MAX_KY/2 + j][dir];
        }
      }
    }
  }
}
#endif

/**
   This function is just a wrapper for memcpy.
   Copy matrix 1 into matrix2.
   Lots of potential buffer overflows here. That means
   possible security breachs.
   Please do not use this code for evil purposes.
   Use it for good science.
@param[in] matrix1  Source matrix.
@param[out] matrix2  Destination matrix.
*/
void copy_matrices(double matrix1[MAX_KX][MAX_KY][2],
  double matrix2[MAX_KX][MAX_KY][2])
{
  memcpy(matrix2, matrix1, MAX_KX*MAX_KY*2*sizeof(double));
}

/**
   Computes the Poincare surface of section using the Henon method.
   The Poincare section is set at Re{ v(k = (8, 2)) } = -0.0002
   as in Miranda et al. (2013).
@param[in, out] v  Matrix of size MAX_KX*MAX_KY*2 representing the complex coefficients of the velocity in Fourier space.
@param[in] v_previous  Value of v in the previous step
@param[in] e  The unitary e vectors.
@param[in] f  Matrix with forcing on each Fourier mode
@param[in, out] t  Time
@param[in] t_previous  Value of time in the previous step
@param[in] N_pid  number of parallel processes
@param[in] my_pid  ID of current parallel process
@param[in] init_x  Initial position of parallelized matrix v in the X direction
@param[in] end_x  End position of parallelized matrix v in the X direction
@param[in] init_y  Initial position of parallelized v matrix in the Y direction
@param[in] end_y  End position of parallelized matrix v in the Y direction
@param[in] columns  Parallelized columns of matrix v.
@param[in] fp4  Pointer to output file
*/
void compute_poincare_section(double v[MAX_KX][MAX_KY][2],
  double v_previous[MAX_KX][MAX_KY][2], double e[MAX_KX][MAX_KY][2],
  double f[MAX_KX][MAX_KY][2], double *t, double t_previous,
  int N_pid, int my_pid, int init_x, int end_x, 
  int init_y, int end_y, 
  MPI_Datatype columns[N_pid], FILE *fp4)
{
  int i, j, m, n, k1, k2;
  int flag_poincare;
  double poincare_map_step;

  // Indexes for the matrix
  m = 8;
  n = 2 + MAX_KY/2;

//  if(v_previous[m][n][0] < 0.0575 && v[m][n][0] > 0.0575)
//  if(v_previous[m][n][1] < 0.058 && v[m][n][1] > 0.058)
  if(v_previous[m][n][0] < -0.0002 && v[m][n][0] > -0.0002)
  {
    // Copy v_previous into v
    copy_matrices(v_previous, v);

    *t = v_previous[MAX_KX-1][MAX_KY-1][0];

    poincare_map_step = -0.0002 - v_previous[m][n][0];
//    poincare_map_step = 0.058 - v_previous[m][n][1];
//    *t = t_previous + poincare_map_step;

    rk4_parallel(v, *t, poincare_map_step, e, f, N_pid, my_pid, 
      init_x, end_x, init_y, end_y, 1, columns);

    *t = v[MAX_KX-1][MAX_KY-1][0];

    if(my_pid == 0)
    {
//      Man, storing your results in plain text will devour your hard drive.
//      fprintf(fp4, "%lf %12.11lf %12.11lf\n", *t, v[m][n][0], v[m][n][1]);
//      fprintf(stdout, "%lf %12.11lf %12.11lf\n", *t, v[m][n][0], v[m][n][1]);
      fwrite(t, sizeof(double), 1, fp4);
      fwrite((void *)v, sizeof(double), MAX_KX*MAX_KY*2, fp4);
    }
  }
}
