#include <stdio.h>

#define MAX_KX 9
#define MAX_KY 17

main(int argc, char **argv)
{
  int i, j;
  char filename_output[20]; // Yes, it's fixed, don't bother me
  double v[MAX_KX][MAX_KY][2];
  double t;
  FILE *f_input, *f_output;

  if(argc < 2)
  {
    fprintf(stderr, "Usage: %s <kx_wavenumber> <ky_wavenumber>\n", argv[0]);
  }
  else
  {
    i = atoi(argv[1]);
    j = atoi(argv[2]);
    sprintf(filename_output, "poincare_%d_%d.dat", i, j);

    if(!(f_input = fopen("../fourier_space.dat", "r")))
    {
      fprintf(stderr, "Error opening file ../fourier_space.dat\n");
    }
    else if(!(f_output = fopen(filename_output, "w")))
    {
      fprintf(stderr, "Error opening file %s\n", filename_output);
    }
    else
    {

      // Corrige j
      j = j + MAX_KY/2;
      while(!feof(f_input))
      {
        fread(&t, sizeof(double), 1, f_input);
        fread((void *)v, sizeof(double), MAX_KX*MAX_KY*2, f_input);

        fprintf(f_output, "%lf %12.11lf %12.11lf\n", t,
           v[i][j][0], v[i][j][1]);

      }
      fclose(f_input);
      fclose(f_output);
    }
  }

  return 0;
}
