#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h> // man 3 memset
#include <fftw3.h>

// Dimensions of the Z^2 Subspace
// See Fig. 1 from Lee (1995)
#define MAX_KX 9
#define MAX_KY 17

#define FACTOR_INTERP 16

#define MAX_KX_INTERP (FACTOR_INTERP*(MAX_KX-1) + 1)
#define MAX_KY_INTERP (FACTOR_INTERP*(MAX_KY-1) + 1)
//#define MAX_KX_INTERP 33
//#define MAX_KY_INTERP 65

#define M (FACTOR_INTERP*(MAX_KY-1))
#define N (FACTOR_INTERP*(MAX_KY-1))

//#define M_INTERP (4*(M-1))
//#define N_INTERP (4*(N-1))

//#define M_INTERP 512
//#define N_INTERP 512

#define M_INTERP M
#define N_INTERP N

#define TRUNCATION 8

// Definition of the subspace as described by Feudel & Seehafer (1995)
// K^2 := {k e Z^2 : k1 > 0} U {k e Z^2 : k1 = 0 & k2 > 0}
int is_in_subspace(int k1, int k2)
{
  if((k1 > 0) || (k1 == 0 && k2 > 0))
    return 1;
  return 0;
}
                                                                                
// Definition of the truncation in the K^2 space by Feudel & Seehafer (1995)
// The K^2 subspace is segmented into successive rings defined by
// n^2 - n < k^2 <= n^2 + n
int is_in_truncation(int k1, int k2)
{
  if(k1*k1 + k2*k2 <= TRUNCATION*TRUNCATION + TRUNCATION)
    return 1;
  return 0;
}

// Initialization of the unit 'polarization' vectors e_k
// (following Lee (1987))
void initialize_e_vectors(double e[MAX_KX][MAX_KY][2])
{
  int i, j, k;
  int k1, k2;
                                                                                
  double norm;
                                                                                
  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      for(k = 0; k < 2; k++)
      {
        e[i][j][k] = 0.;
      }
    }
  }
                                                                                
  for(i = 0; i < MAX_KX; i++)
  {
    for(j = 0; j < MAX_KY; j++)
    {
      // k vector
      k1 = i;
      k2 = j - MAX_KY/2;
                                                                                
                                                                                
      if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
      {
        // Orthogonality
        e[i][j][0] = k2;
        e[i][j][1] = -k1;
                                                                                
        // Unitary
        norm = sqrt(k1*k1 + k2*k2);
                                                                                
        e[i][j][0] = e[i][j][0]/norm;
        e[i][j][1] = e[i][j][1]/norm;
                                                                                
      }
    }
  }
}

// Perform matrix shifting for the ifft routine
//void fftshift(double v[MAX_KX][MAX_KY][2], double v_shifted[MAX_KY][MAX_KX][2],
void fftshift(double v[MAX_KX][MAX_KY][2], double v_shifted[MAX_KY_INTERP][MAX_KX_INTERP][2],
    double e[MAX_KX][MAX_KY][2], int dir)
{
  int i, j, k;
  int k1, k2;
                                                                                
  memset(v_shifted, 0, MAX_KY_INTERP*MAX_KX_INTERP*2*sizeof(double));
                                                                                
  // MAX_KY is odd
  if(MAX_KY % 2 == 1)
  {
    for(i = 0; i < MAX_KX; i++)
    {
      for(j = 0; j < MAX_KY; j++)
      {
        k1 = i;
        k2 = j - MAX_KY/2;
                                                                                
        if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
        {
          for(k = 0; k < 2; k++)
          {
//            v_shifted[(MAX_KY/2 + 1 + j)%MAX_KY][i][k] = v[i][j][k]*e[i][j][dir];
            v_shifted[(MAX_KY_INTERP - MAX_KY/2 + j)%MAX_KY_INTERP][i][k] = v[i][j][k]*e[i][j][dir];
          }
        }
//	else
//        {
//          for(k = 0; k < 2; k++)
//          {
//            v_shifted[(MAX_KY_INTERP - MAX_KY/2 + j)%MAX_KY_INTERP][i][k] = 0.;
//          }
//        }
      }
    }

    for(i = 0.; i < MAX_KY_INTERP/2; i++)
    {
      for(j = 0.; j < MAX_KX_INTERP; j++)
      {
//          vx_fourier_space[i][j][0] = 1.;
//          vx_fourier_space[i][j][1] = 1.;
        if(i >= 1 && j == 0)
        {
          v_shifted[MAX_KY_INTERP-i][0][0] = v_shifted[i][j][0];
          v_shifted[MAX_KY_INTERP-i][0][1] = -v_shifted[i][j][1];
        }
      }
    }

    v_shifted[0][0][0] = 0.;
    v_shifted[0][0][1] = 0.;
  }
  else // MAX_KY is even
  {
    for(i = 0; i < MAX_KX; i++)
    {
      for(j = 0; j < MAX_KY/2; j++)
      {
        for(k = 0; k < 2; k++)
        {
          v_shifted[MAX_KY/2 - j - 1][i][k] = v[i][j][k]*e[i][j][dir];
          v_shifted[MAX_KY - j - 1][i][k]
            = v[i][MAX_KY/2 + j][k]*e[i][MAX_KY/2 + j][dir];
        }
      }
    }
  }
}


// Perform matrix shifting for the ifft routine
void fftshift_simple(double v[MAX_KX][MAX_KY][2], double v_shifted[MAX_KY_INTERP][MAX_KX_INTERP][2])
{
  int i, j, k;
  int k1, k2;
                                                                                
  memset(v_shifted, 0, MAX_KY_INTERP*MAX_KX_INTERP*2*sizeof(double));
                                                                                
  // MAX_KY is odd
  if(MAX_KY % 2 == 1)
  {
    for(i = 0; i < MAX_KX; i++)
    {
      for(j = 0; j < MAX_KY; j++)
      {
        k1 = i;
        k2 = j - MAX_KY/2;
                                                                                
        if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
        {
          for(k = 0; k < 2; k++)
          {
//            v_shifted[(MAX_KY/2 + 1 + j)%MAX_KY][i][k] = v[i][j][k];
            v_shifted[(MAX_KY_INTERP - MAX_KY/2 + j)%MAX_KY_INTERP][i][k] = v[i][j][k];
          }
        }
	else
        {
          for(k = 0; k < 2; k++)
          {
            v_shifted[(MAX_KY_INTERP - MAX_KY/2 + j)%MAX_KY_INTERP][i][k] = 0.;
          }
        }
      }
    }
    for(i = 0.; i < MAX_KY_INTERP/2; i++)
    {
      for(j = 0.; j < MAX_KX_INTERP; j++)
      {
//          vx_fourier_space[i][j][0] = 1.;
//          vx_fourier_space[i][j][1] = 1.;
        if(i >= 1 && j == 0)
        {
          v_shifted[MAX_KY_INTERP-i][0][0] = v_shifted[i][j][0];
          v_shifted[MAX_KY_INTERP-i][0][1] = -v_shifted[i][j][1];
        }
      }
    }

    v_shifted[0][0][0] = 0.;
    v_shifted[0][0][1] = 0.;

  }
  else // MAX_KY is even
  {
    for(i = 0; i < MAX_KX; i++)
    {
      for(j = 0; j < MAX_KY/2; j++)
      {
        for(k = 0; k < 2; k++)
        {
          v_shifted[MAX_KY/2 - j - 1][i][k] = v[i][j][k];
          v_shifted[MAX_KY - j - 1][i][k]
            = v[i][MAX_KY/2 + j][k];
        }
      }
    }
  }
}


int main()
{
  int i, j;
  int k1, k2;

  double t;
  double v[MAX_KX][MAX_KY][2];

  double e[MAX_KX][MAX_KY][2];
  double vx_fourier_space[MAX_KY_INTERP][MAX_KX_INTERP][2];
  double vy_fourier_space[MAX_KY_INTERP][MAX_KX_INTERP][2];
  double vx_real_space[M_INTERP][N_INTERP], vy_real_space[M_INTERP][N_INTERP];
//  double *vx_real_space, *vy_real_space;

  double vort_fourier[MAX_KX][MAX_KY][2];
  double temp_vort_fourier[MAX_KY_INTERP][MAX_KX_INTERP][2];
  double vort[M_INTERP][N_INTERP];
  double okuboweiss[M_INTERP][N_INTERP];
//  double *vort;

  double sum_real, sum_fourier, sum_enstrophy_real, sum_enstrophy_fourier;
  double sum_s1, sum_s2, sum_total_strain;
  double okubo_weiss_s1_fourier[MAX_KX][MAX_KY][2];
  double okubo_weiss_s2_fourier[MAX_KX][MAX_KY][2];
  double temp_okubo_weiss_s1_fourier[MAX_KY_INTERP][MAX_KX_INTERP][2];
  double temp_okubo_weiss_s2_fourier[MAX_KY_INTERP][MAX_KX_INTERP][2];
  double s1[M_INTERP][N_INTERP], s2[M_INTERP][N_INTERP];

  double std_okuboweiss, skewness_okuboweiss;


  double dx, dy;

  FILE *fp1, *fp2, *fp5, *fp6;


  fftw_plan p1, p2, p3, p4, p5;

    if(!(fp1 = fopen("../fourier_space.dat", "r")))
    {
      fprintf(stderr, "Error opening input file fourier_space.dat\n");
      return 0;
    }

    if(!(fp2 = fopen("real_space.dat", "w")))
    {
      fprintf(stderr, "Error opening output file vector_real_space.dat\n");
      return 0;
    }
    if(!(fp5 = fopen("vorticity.dat", "w")))
    {
      fprintf(stderr, "Error opening output file vorticity.dat\n");
      return 0;
    }
    if(!(fp6 = fopen("quantities.dat", "w")))
    {
      fprintf(stderr, "Error opening output file quantities.dat\n");
      return 0;
    }

//      vort = (double *)malloc(N_INTERP*M_INTERP*sizeof(double));
//      vx_real_space = (double *)malloc(N_INTERP*M_INTERP*sizeof(double));
//      vy_real_space = (double *)malloc(N_INTERP*M_INTERP*sizeof(double));

      // Initialization of the fftw library
      p1 = fftw_plan_dft_c2r_2d(M_INTERP, N_INTERP, &(vx_fourier_space[0][0]),
        &(vx_real_space[0][0]), FFTW_ESTIMATE);
//        &vx_real_space[0], FFTW_MEASURE);
                                                                                
      p2 = fftw_plan_dft_c2r_2d(M_INTERP, N_INTERP, &(vy_fourier_space[0][0]),
        &(vy_real_space[0][0]), FFTW_ESTIMATE);
                                                                                
      p3 = fftw_plan_dft_c2r_2d(M_INTERP, N_INTERP, &(temp_vort_fourier[0][0]),
        &(vort[0][0]), FFTW_ESTIMATE);

      p4 = fftw_plan_dft_c2r_2d(M_INTERP, N_INTERP, &(temp_okubo_weiss_s1_fourier[0][0]),
        &(s1[0][0]), FFTW_ESTIMATE);

      p5 = fftw_plan_dft_c2r_2d(M_INTERP, N_INTERP, &(temp_okubo_weiss_s2_fourier[0][0]),
        &(s2[0][0]), FFTW_ESTIMATE);


      initialize_e_vectors(e);

      dx = 2.*M_PI/M_INTERP;
      dy = 2.*M_PI/N_INTERP;

      while(!feof(fp1))
//      t = 10000.;
//      while(t < 10101.)
      {

        for(i = 0.; i < MAX_KX; i++)
        {
          for(j = 0.; j < MAX_KY; j++)
          {
            v[i][j][0] = 0.;
            v[i][j][1] = 0.;
          }
        }

        fread(&t, sizeof(double), 1, fp1);
        fread((void *)v, sizeof(double), MAX_KX*MAX_KY*2, fp1);

        fprintf(stderr, "t = %lf\n", t);

        p1 = fftw_plan_dft_c2r_2d(M_INTERP, N_INTERP, &vx_fourier_space[0][0],
          &vx_real_space[0][0], FFTW_ESTIMATE);
                                                                                
        p2 = fftw_plan_dft_c2r_2d(M_INTERP, N_INTERP, &vy_fourier_space[0][0],
          &vy_real_space[0][0], FFTW_ESTIMATE);
                                                                                
        p3 = fftw_plan_dft_c2r_2d(M_INTERP, N_INTERP, &temp_vort_fourier[0][0],
          &vort[0][0], FFTW_ESTIMATE);

        // Write the fluid state in real space
                                                                                
        // Initialize the matrices for the FFTW library
        fftshift(v, vx_fourier_space, e, 0);
        fftshift(v, vy_fourier_space, e, 1);

        sum_fourier = 0.;

        for(i = 0.; i < MAX_KY_INTERP/2; i++)
        {
          for(j = 0.; j < MAX_KX_INTERP; j++)
          {
//          vx_fourier_space[i][j][0] = 1.;
//          vx_fourier_space[i][j][1] = 1.;
          if(i >= 1 && j == 0)
            {
              vx_fourier_space[MAX_KY_INTERP-i][0][0] = vx_fourier_space[i][j][0];
              vx_fourier_space[MAX_KY_INTERP-i][0][1] = -vx_fourier_space[i][j][1];
              vy_fourier_space[MAX_KY_INTERP-i][0][0] = vy_fourier_space[i][j][0];
              vy_fourier_space[MAX_KY_INTERP-i][0][1] = -vy_fourier_space[i][j][1];
            }
	  }
        }

        vx_fourier_space[0][0][0] = 0.;
        vx_fourier_space[0][0][1] = 0.;
        vy_fourier_space[0][0][0] = 0.;
        vy_fourier_space[0][0][1] = 0.;

        for(i = 0.; i < MAX_KY_INTERP; i++)
        {
          for(j = 0.; j < MAX_KX_INTERP; j++)
          {
            if(!(i > MAX_KY_INTERP/2 && j == 0))
            sum_fourier = sum_fourier + vx_fourier_space[i][j][0]*vx_fourier_space[i][j][0] + vx_fourier_space[i][j][1]*vx_fourier_space[i][j][1] + vy_fourier_space[i][j][0]*vy_fourier_space[i][j][0] + vy_fourier_space[i][j][1]*vy_fourier_space[i][j][1];
          }
        }
                                                                                
        sum_enstrophy_fourier = 0.;
        // Calculate vorticity in Fourier space
        for(i = 0; i < MAX_KX; i++)
        {
          for(j = 0; j < MAX_KY; j++)
          {
            k1 = i;
            k2 = j - MAX_KY/2;
                                                                                
            if(is_in_subspace(k1, k2) && is_in_truncation(k1, k2))
            {
              vort_fourier[i][j][0] = -(k1*e[i][j][1] - k2*e[i][j][0])*v[i][j][1];
              vort_fourier[i][j][1] =  (k1*e[i][j][1] - k2*e[i][j][0])*v[i][j][0];
              sum_enstrophy_fourier = sum_enstrophy_fourier + (vort_fourier[i][j][0]*vort_fourier[i][j][0]) + (vort_fourier[i][j][1]*vort_fourier[i][j][1]);

              okubo_weiss_s1_fourier[i][j][0] = -(k1*e[i][j][0] - k2*e[i][j][1])*v[i][j][1];
              okubo_weiss_s1_fourier[i][j][1] = (k1*e[i][j][0] - k2*e[i][j][1])*v[i][j][0];

              okubo_weiss_s2_fourier[i][j][0] = -(k1*e[i][j][1] + k2*e[i][j][0])*v[i][j][1];
              okubo_weiss_s2_fourier[i][j][1] =  (k1*e[i][j][1] + k2*e[i][j][0])*v[i][j][0];

//              sum_fourier = sum_fourier + vy_fourier_space[i][j][0]*vy_fourier_space[i][j][0] + vy_fourier_space[i][j][1]*vy_fourier_space[i][j][1];
            }
          }
        }
                                                                                
        fftshift_simple(vort_fourier, temp_vort_fourier);
        fftshift_simple(okubo_weiss_s1_fourier, temp_okubo_weiss_s1_fourier);
        fftshift_simple(okubo_weiss_s2_fourier, temp_okubo_weiss_s2_fourier);

        // Calculate the inverse fft
        fftw_execute(p1);
        fftw_execute(p2);
        fftw_execute(p3);
        fftw_execute(p4);
        fftw_execute(p5);

        sum_real = 0.;
        sum_enstrophy_real = 0.;
        sum_s1 = 0.;
        sum_s2 = 0.;
        sum_total_strain = 0.;
        std_okuboweiss = 0.;

        for(i = 0; i < M_INTERP; i++)
        {
          for(j = 0; j < N_INTERP; j++)
          {
//            sum_real = sum_real + (vy_real_space[i][j]*vy_real_space[i][j]);
//            sum_real = sum_real + (vy_real_space[i][j]*vy_real_space[i][j]);
//            sum_real = sum_real + (vx_real_space[i][j]*vx_real_space[i][j]
            sum_real = sum_real + (vx_real_space[i][j]*vx_real_space[i][j]
                        +  vy_real_space[i][j]*vy_real_space[i][j]);

            sum_enstrophy_real = sum_enstrophy_real + vort[i][j]*vort[i][j];

            sum_s1 = sum_s1 + s1[i][j]*s1[i][j];
            sum_s2 = sum_s2 + s2[i][j]*s2[i][j];
            sum_total_strain = sum_total_strain + s1[i][j]*s1[i][j] + s2[i][j]*s2[i][j];
            okuboweiss[i][j] = 0.5*(1./(M*N))*s1[i][j]*s1[i][j] + 0.5*(1./(M*N))*s2[i][j]*s2[i][j] - 0.5*(1./(M*N))*vort[i][j]*vort[i][j];
            std_okuboweiss = std_okuboweiss + okuboweiss[i][j]*okuboweiss[i][j];
          }
        }
        std_okuboweiss = sqrt(std_okuboweiss/(M_INTERP*N_INTERP - 1));

        skewness_okuboweiss = 0.;
        for(i = 0; i < M_INTERP; i++)
        {
          for(j = 0; j < N_INTERP; j++)
          {
            skewness_okuboweiss = skewness_okuboweiss + pow(okuboweiss[i][j], 3.)/pow(std_okuboweiss, 3.);
          }
        }

        // Compare quantities computed in fourier and real spaces
        fprintf(fp6, "%lf %lf %lf %lf %lf\n", t, sum_fourier, 0.5*(1./(M*N))*sum_real, sum_enstrophy_fourier, 0.5*(1./(M*N))*sum_enstrophy_real);

/*
        fprintf(fp6, "%lf %lf %lf %lf %lf %lf %lf %lf %lf\n", t, 
                                        0.5*(1./(M*N))*sum_real, 
                              0.5*(1./(M*N))*sum_enstrophy_real, 
                                          0.5*(1./(M*N))*sum_s1, 
                                          0.5*(1./(M*N))*sum_s2, 
                                0.5*(1./(M*N))*sum_total_strain, 
                                   1./(M*N)*skewness_okuboweiss, 
                                                 std_okuboweiss, 
           0.5*(1./(M*N))*(sum_s1 + sum_s2 - sum_enstrophy_real));
*/

//        printf("%lf %lf %lf\n", t, sum_fourier, ((1./(M_PI))) * (sum_real/(MAX_KX*MAX_KY)*(2.*M_PI/M)*(2.*M_PI/M)));
//        printf("%lf %lf %lf\n", t, sum_fourier, 0.5*(1./(2.*M_PI*2.*M_PI))*sum_real);
//        printf("%lf %lf %lf\n", t, sum_fourier, 0.5*sum_real*dx*dy);
//        printf("%lf %lf %lf\n", t, sum_fourier, 0.5*(1./(M*N))*sum_real);
//        printf("%lf %lf %lf\n", t, sum_fourier, sum_real);

//        printf("%lf %lf %lf\n", t, sum_fourier, (sum_real)*(2.*M_PI*2.*M_PI/(M*N)));
//          fftw_execute(p3);
                                                                                
        // Write the results to a file
//        fprintf(fp2, "%lf %lf %lf\n", t, vx_fourier_space[0][0][0], vx_fourier_space[0][0][1]);
          fwrite(&t, sizeof(double), 1, fp2);
          fwrite((void *)vx_real_space, sizeof(double), M_INTERP*N_INTERP, fp2);
          fwrite((void *)vy_real_space, sizeof(double), M_INTERP*N_INTERP, fp2);
//          show_big_matrix(vx_real_space);
          fwrite(&t, sizeof(double), 1, fp5);
          fwrite((void *)vort, sizeof(double), M_INTERP*N_INTERP, fp5);

/*
    for(i = 0; i < M_INTERP; i++)
        {
        for(j = 0; j < N_INTERP; j++)
        {
          printf("%lf %lf %lf %lf\n", dx*(double)i, dy*(double)j, vx_real_space[j][i], vy_real_space[j][i]);
        }
        }
*/


        } // !feof(fp1)

	fclose(fp1);
	fclose(fp2);
	fclose(fp5);
	fclose(fp6);

//        free(vort);
//        free(vx_real_space);
//        free(vy_real_space);

  return 0;
}
