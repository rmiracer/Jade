if [ ! -d ./figs ]; then
  mkdir figs
fi

T_START=1000.2
T_STEP=0.1

START=0
END=451
STEP=2

for i in `seq $START $STEP $END`; do

  echo $i

  SNAPSHOT=`echo $T_START+$T_STEP*$i | bc -l`;

  echo $SNAPSHOT

  ./get_snapshot_vort.exe $SNAPSHOT

  gnuplot plot.gplot

  mv fig.png figs/fig_$SNAPSHOT.png

done
