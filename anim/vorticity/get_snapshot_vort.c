#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define M 256
#define N 256

int main(int argc, char **argv)
{
  int i, j;
  double t, targ, *vort;

  FILE *fp, *fpout;

  vort = NULL;

  if(argc < 2)
  {
    fprintf(stderr, "Usage: %s <snapshot_time>\n", argv[0]);
  }
  else if( !(fp = fopen("../../extract/vorticity.dat", "r")) )
  {
    fprintf(stderr, "Error opening file real_space.dat for reading\n");
  }
  else if( !(fpout = fopen("snapshot_vort.dat", "w")) )
  {
    fprintf(stderr, "Error opening file snapshot_vort.dat for writing\n");
  }
  else
  {
    targ = atof(argv[1]);

    vort = (double *)malloc(sizeof(double)*M*N);

    t = 0.;

    while( !feof(fp) && t < targ)
    {
      fread(&t, sizeof(double), 1, fp);
      fread(vort, sizeof(double), M*N, fp);
    }

    // We allow for a small difference between t and targ
    if( !(fabs((t - targ)) <= 0.1) || feof(fp) )
    {
      fprintf(stderr, "Error: snapshot at time t = %lf not found\n", targ);
      fprintf(stderr, "       (targ = %lf, t = %lf)\n", targ, t);
    }
    else
    {
      fprintf(stdout, "t = %lf\n", t);

      for(i = 0; i < M; i++)
      {
        for(j = 0; j < N; j++)
        {
          fprintf(fpout, "%lf %lf %lf\n", 2.*M_PI*((double)j/M),
                                            2.*M_PI*((double)i/N),
                                            vort[i*N + j]);
        }
      }
    }

    free(vort);
    vort = NULL;
    fclose(fp);
    fclose(fpout);
  }

  return 0;
}
