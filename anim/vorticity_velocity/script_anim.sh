if [ ! -d ./figs ]; then
  mkdir figs
fi

T_START=615.0
T_STEP=0.1

START=0
END=400
STEP=10

j=0

for i in `seq $START $STEP $END`; do

  echo $i

  SNAPSHOT=`echo $T_START+$T_STEP*$i | bc -l`;

  echo $SNAPSHOT > time.dat

  ./get_snapshot_vort.exe $SNAPSHOT 0.1

  ./get_snapshot_velocity.exe $SNAPSHOT 0.1

  gnuplot plot.gplot

  SNAPFILE=`echo $j | awk '{printf "%05d", $1}'`

  mv fig.png figs/fig_$SNAPFILE.png

  j=$((j+1))
done
